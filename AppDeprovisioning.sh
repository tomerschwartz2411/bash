#!/bin/bash

#Checking if software directory exist, if it does; remove it.
function check_dir_existence() {
  if [ -d "/opt/someapp" ]; then
    echo "app directory exist, deleting directory"
    rm -rf /opt/someapp
  else
    echo "someapp directory doesn't exist, skipping..."
  fi
}

#Checking if app service exist, if it does; remove it.
function removing_someappservice1() {
  service="someapp1"
  if [ -f "/etc/systemd/system/someapp1.service" ]; then
    echo "someapp1 service exist, removing service"
    systemctl stop someapp1
    systemctl disable someapp1
    rm /etc/systemd/system/someapp1.service
  else
    echo "someapp1 service doesn't exist, skipping..."
  fi
}

#Checking if someapp2 service exist, if it does; remove it.
function removing_someappservice2() {
  # shellcheck disable=SC2034
  service="someapp2"
  if [ -f "/etc/systemd/system/someapp2.service" ]; then
    echo "someapp2 service exist, removing service"
    systemctl stop someapp2
    systemctl disable someapp2
    rm /etc/systemd/system/someapp2.service
  else
    echo "someapp2 service doesn't exist, skipping..."
  fi
}

#Reloading systemctl configuration and resetting failed services in order to get rid of possible residues of the software.
function daemonreload_resetfailed() {
  systemctl daemon-reload
  systemctl reset-failed
}
check_dir_existence
echo "checking if app1&2 related services exist..."
removing_someappservice1
removing_someappservice2
daemonreload_resetfailed
